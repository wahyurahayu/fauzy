const Colors = {
  white: '#FFF',
  purple1: '#24A19C',
  purple2: '#24A19C',
  purple3: '#24A19C',
  grey1: '#24A19C',
  grey2: '#868AA3',
  grey3: '#F9F5FB',
  line: '#F6F8FA'
};

export default Colors;
