import {StackActions} from '@react-navigation/native';
import React, {useEffect} from 'react';
import {View, Image} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

interface SplashProps {
  navigation: any;
}

const Splash = (props: SplashProps) => {
  useEffect(() => {
    setTimeout(() => {
      props.navigation.dispatch(StackActions.replace('Awal'));
    }, 3000);
  });
  return (
    <ScrollView>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          height: 780,
          backgroundColor: '#24A19C',
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            height: 780,
            
            }}>
          <Image
            source={require('../gambar/bgg.jpg')}
            style={{
              width: 355,
              height: 780,
            }}
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default Splash;
